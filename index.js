const express = require('express');
const admin = require('firebase-admin');
const serviceAccount = require('./supermarkets-products-index-firebase-adminsdk-jnupe-800b1722ce.json');
const PORT = 5003;
let USE_INITIAL_STATE = process.env.USE_INITIAL_STATE || false;
const INVOICES = 'invoices';
const PRODUCTS = 'products';
const EANS = 'eans';
const ITENS_ERRORS = 'non-processed-itens';
let cosmos = require('bluesoft-cosmos-api');

// Consiga seu Token em: https://cosmos.bluesoft.com.br
cosmos.setToken('ivV6M53f-ZSxlYfT9nQlLA');

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
});

const db = admin.firestore();

const getProduct = (db, ean) => {
    return db.collection(PRODUCTS).doc(ean).get();
}

const getProductInfo = (db, ean) => {
    return db.collection(EANS).doc(ean).get();
}

const getProductValue = (invoiceItem) => {
    return invoiceItem.desconto > 0
        ? (invoiceItem.valor - invoiceItem.desconto) / invoiceItem.qtd
        : invoiceItem.valorUnitario;
}

const getProductAvgPrice = (prices) => {
    const sum = prices.map(price => price.valor)
        .reduce((acc, valor) => acc + valor, 0)
    return (sum / prices.length);
};

const createProduct = (newInvoice, invoiceItem, productInfo) => {
    const newProduct = {
        ncm: invoiceItem.ncm,
        ean: invoiceItem.eanComercial,
        unidade: invoiceItem.unidadeComercial,
        descricao: invoiceItem.descricao,
        precoMedio: getProductValue(invoiceItem),
        precoMin: getProductValue(invoiceItem),
        emitentePrecoMin: newInvoice.emitente,
        precoMax: getProductValue(invoiceItem),
        emitentePrecoMax: newInvoice.emitente,
        prices: [
            {
                valor: getProductValue(invoiceItem),
                data: newInvoice.data,
                emitente: newInvoice.emitente
            }
        ],
        ultimoCupom: newInvoice.chaveAcesso
    };

    // data from cosmos, but already in firebase
    newProduct['descricaoAmigavel'] = productInfo.description;
    if (productInfo.thumbnail){
        newProduct['thumbnail'] = productInfo.thumbnail;
    }
    if (productInfo.brand){
        newProduct['brand'] = productInfo.brand;
    }

    return db
        .collection(PRODUCTS)
        .doc(newProduct.ean)
        .set(newProduct);
};

const updateProduct = (newInvoice, invoiceItem, product) => {
    const preco = getProductValue(invoiceItem);
    product.prices.push({
        valor: preco,
        data: newInvoice.data,
        emitente: newInvoice.emitente
    });
    product.precoMedio = getProductAvgPrice(product.prices);
    if (preco < product.precoMin) {
        product.precoMin = preco;
        product.emitentePrecoMin = newInvoice.emitente;
    } else if (preco > product.precoMax) {
        product.precoMax = preco;
        product.emitentePrecoMax = newInvoice.emitente;
    }

    return admin
        .firestore()
        .collection(PRODUCTS)
        .doc(product.ean)
        .set(product);
};

const saveNonProcessedItem = (db, item, chaveAcesso) => {
    item['chaveAcesso'] = chaveAcesso;
    db.collection(ITENS_ERRORS).add(item);
}

async function listenToProducts() {
    db.collection(INVOICES).onSnapshot(snapshot => {
        if (USE_INITIAL_STATE === true){
            console.log('got initial state');
            snapshot.docChanges.forEach(change => {
                if (change.type !== 'removed'){
                    const invoice =  change.doc.data();
                    if (invoice.type !== undefined && invoice.type === 'SUPERMERCADO'){
                        // console.log(`invoice ${change.type}: `, invoice.chaveAcesso);
                        invoice.produtos.forEach(async (item) => {
                            const product = await getProduct(db, item.eanComercial);
                            if (product && product.exists){
                                const productData = product.data();
                                console.log(`product ${productData.descricao} exists `);
                                updateProduct(invoice, item, productData);
                            }else {

                                const productInfo = await getProductInfo(db, item.eanComercial);
                                if (productInfo && productInfo.exists){
                                    console.log(`creating product with existing EAN ${item.descricao} (${item.eanComercial}) from ${invoice.chaveAcesso}`);
                                    const created = await createProduct(invoice, item, productInfo.data());
                                }else {
                                    try {
                                        console.log(`Trying to get from cosmos ${item.descricao} (${item.eanComercial})`);
                                        const cosmosInfo = await cosmos.gtins(item.eanComercial);
                                        if (cosmosInfo.status === 200) {
                                            db.collection(EANS).doc(item.eanComercial).set(cosmosInfo.data);
                                            console.log(`creating product with NON existing EAN ${item.descricao} (${item.eanComercial}) from ${invoice.chaveAcesso}`);
                                            const created = await createProduct(invoice, item, cosmosInfo.data);
                                        }else {
                                            console.log(`Error getting cosmos data NON 200: `, error);
                                            saveNonProcessedItem(db, item, invoice.chaveAcesso);
                                        }
                                    } catch (error) {
                                        console.log(`Error getting cosmos data: ${item.eanComercial} - ${item.descricao}`, error);
                                        saveNonProcessedItem(db, item, invoice.chaveAcesso);
                                    }
                                }
                            }
                        });
                    }
                }
            });
        }else {
            console.log('skiped initial state');
            USE_INITIAL_STATE = true;
        }
    });
}

listenToProducts();

// express basic init
const app = express();
app.get('/', (req, res) => {
    res.send('Mais Barato Products Service is alive');
});
app.listen(PORT, () => console.log(`Mais Barato Products Service listening on port ${PORT}!`))